# Notes

First I read through the problem. Seems to be a binary tree subtree detection problem (but not a BST, not full, not complete)

The given implementation constructs a string by appending a `DomNode`'s `value` attribute by pre-order traversal of the tree, then decides by a non-negative `indexOf` return whether one string is a substring of the other

# Questions

## 1. This code will not work in certain scenarios. Identify the scenarios it will not work for.

The given implementation serializes a tree to a string by appending each `DomNode`'s `value` attribute by pre-order traversal of the tree (blank string for `null`, then decides by a non-negative `indexOf` return whether a second tree can be found within it by testing whether the second tree's traversal string is a substring of the other. I'll refer to these trees as "main" tree and "test" tree below.

The first issue with this system is that it has only been tested on cases where `DomNode.value` is a single character (other than the "root" node). Since there are no characters separating each node, it would be impossible to tell the beginning or end of each node, except for the beginning of the root node. This can be trivially solved with a unique separator character such as a space (" ") without damaging the current implementation which uses `indexOf` (i.e., if a subtree was correctly found before, it will continue to be). 

_Note_: We'll have to make the assumption that whitespace would not normally appear in a `DomNode.value` string. If whitespace were valid, we could switch to a different character. If no character were sufficient, then we would have to use a different data structure than a string to serialize the traversal, e.g., an array.

The second issue with the current solution is false positives. Currently, `stringFromPreorder()` can produce identical output given different input. e.g., if you have a main tree a=>{b} and a test tree a=>{null, b} they will produce the same pre-order traversal string "ab". This happens not just when left and right child have been ordered differently; a main tree a=>{b,c} and a test tree a=>{b=>{c}} will also produce the same pre-order traversal string "abc"

This stems from a well-known property (researched, of course) of binary trees stating that to reconstruct a binary tree, you need a specific pair of traversals (pre-/in-order, post-/in-order, or level-/in-order) to generate the original tree in the same way. Additional research has shown that one can use a single traversal with modifications for null nodes but I have not found a proof to this effect.


## 2. What would you add or modify to this algorithm to make it work?

I'll start by listing a few assumptions
- we cannot assume these are binary search trees
- duplicate nodes are not possible
- `DomNode.value` will never be/include whitespace (see reasoning in #1 above)
- the leaf nodes of the test tree do not need to be leaves in the main tree. 
  - e.g., it would be okay if a test tree b=>{c} were to be found embedded in a main tree a=>{b=>{c=>d}} (c is a leaf in the former; in the latter it is a node with child leaf d)
- node order matters. 
  - e.g., if a node is on the left in the test tree, it must also be in the main tree

One assumption I was hesitant about is whether null nodes in the test tree can be non-null in the main tree. For example, should a main tree a=>{b,c} have test tree a=>{null, c} as a subtree? Since I've already assumed this for leaf nodes of the test tree, above, I decided to do so for nodes as well. I think this makes sense since the main goal in finding vdom in dom would be a test for _presence_ of nodes in a particular arrangement, and not necessarily a test for _absence_ of nodes.

All that having been said, a correct solution would have to stop emitting the false positives as highlighted above, while finding subtrees which weren't found by the original solution (e.g., main tree a=>{b, c} and test tree a=>{null, c} serialize to "abc" and "ac" which fails in the current algorithm, but should not fail according to assumptions).

Based on the above, I chose to try a modified preorder traversal

- In the serialization, store a null character (I'm using 0 in the code for simplicity) everytime a null node is found. This will make sure left and right nodes are always in the right place. Use a space character to separate nodes in general. 
- In the detection, scan each serialization independently until reaching the end of one or the other. At each step iterate like so:
  - if the test token is null or both tokens are equal *=>grab the next token from both trees*
  - else if the tokens don't match *=>grab the next token only in the main tree*
- At the end of scanning, 
  - if all of the test tree tokens were scanned *=> subtree found*


## 3. Write a few test cases that show that your new code covers different scenarios.

Algorithms and test cases in `q1.ts`

The results in the test case skew in my favour but I am hesitant to say I have solved the problem entirely. I saw no general proof online that said a marked-up preorder traversal could definitively produce the correct tree so it stands to reason that the solution I used and the test cases I chose simply miss the mistake.


## 4. Now, in the real world, the DOM is a tree with an arbitrary number children on each node. How would you modify this code, which is currently for a binary tree, to make it work for a k-ary tree, where k is the maximum number of children a node can have?

Alright so for this one I am going to continue headlong into assuming there's some preorder solution that can work out. 

I'll make a few assumptions, similar to before, but modified:
- we cannot assume these are search trees
- duplicate nodes are not possible
- `DomNode.value` will never be whitespace or "0" like before
- the leaf nodes of the test tree do not need to be leaves in the main tree. 
- node order matters.
  - e.g., if a node is in position 3 in the test tree, it must be in position 3 in the main tree

Some additional assumptions are:
- the main tree and the test tree must be of the same k-ariness. i.e., I won't be checking 10-trees to see if they have a 5-tree subtree. while one could make an argument about ignoring nulls to make this work, I want to reduce the complexity of the algorithm and testing for the sake of completing this assignment in time
- it's ok to store the value of k in the traversal and expect it is there for deserialization (as opposed to computing k during deserialization)

To modify this solution for a k-ary tree, I'll have to make a few changes:

- the `serializePreorder` and `treeDeserializer` functions will have to iterate k children instead of just a left and right child
  - I've decided that a simple way to solve how to know the value for k is to store it as the first token in the traversal just before the root node.
- i'll include the unit tests from the previous question but I'll have to tackle fewer new test cases in general since the problem is very generalized. The newer tests can be found by skipping to the section of `q4.ts` named "BEGIN K-ARY TREE TEST SECTION!"

Implementation and test cases in `q4.ts`

As before, while my unit tests seem to show success I am hesitant about declaring a perfect algorithm without time alotted to formally prove what I've created. 

## 5. Constructing strings and doing string comparisons is immensely expensive, both memory-wise and computation-wise. Devise an algorithm other than comparing generated strings to determine if a k-ary tree is a subtree of another k-ary tree. 

One goal I have is basically to copy `q4.ts` and only replace the `isSubtree` function with a version which does not rely on serialization. It presently takes as input two trees in `DomNode` format. I should be able to replace the portion which iterates over serialized tokens with an alternate method and still be able to re-use all the serialization/deserialization required for the test cases.

One method for accomplishing this would be to do a depth-first-search (DFS) with known linear search time (nodes + edges) and storage (nodes) when looking for a particular node. This assumes the number of nodes in the graph can be searched in a reasonable amount of time. I am unsure if this is something which can be taken for granted with DOM trees. Clearly there are DOM renderers which are highly optimized, and have many person-years of effort put into them, so they must be complex. At the same time, I assume most of the complexity lies in layout engines, layering, and DOM diffing as opposed to searching for subtrees. 

Previous Assumptions Carried Over:
- we cannot assume these are search trees
- duplicate nodes are not possible
- the leaf nodes of the test tree do not need to be leaves in the main tree. 
- node order matters
- the main tree and the test tree must be of the same k-ariness

New Assumptions:
- A DFS is a reasonable approach to determine if the root node of a test tree is in the main tree

An additional function (after the depth-first search function) which I will write is a preorder traversal algorithm which will traverse the candidate subtree (from the DFS step) and test tree in tandem. If at any step the trees don't match (using the same "test for presence" method as before) the algorithm will bail early with a false result. A positive result will be when the test tree has been fully traversed. 

Implementation and test cases in `q5.ts`

The test cases from `q4.ts` pass as well in `q5.ts`. Given additional time I would write more unit test cases as well as potentially a program to generate trees of many different values for k. I would definitely not put this code into production. At any rate, I highly doubt that DOM trees typically fit into such a neat problem space so even if this code were better it would probably need to tackle a number of corner cases I have missed here. More research (as usual) would be necessary.

# References

During the course of this assignment I refreshed on some graph theory. Here are a list of websites I consulted along the way.

## StackExchange sites

https://stackoverflow.com/questions/16381294/determine-if-a-binary-tree-is-subtree-of-another-binary-tree-using-pre-order-and#16381658

https://stackoverflow.com/questions/14287980/checking-subtrees-using-preorder-and-inorder-strings

https://stackoverflow.com/questions/30556590/how-does-inorderpreorder-construct-unique-binary-tree

https://math.stackexchange.com/questions/271613/why-is-a-function-defined-as-having-only-one-y-value-output#271615

https://cs.stackexchange.com/questions/44820/what-does-pre-post-and-in-order-walk-mean-for-a-n-ary-tree

https://cs.stackexchange.com/questions/439/which-combinations-of-pre-post-and-in-order-sequentialisation-are-unique

https://cs.stackexchange.com/questions/11015/constructing-a-binary-tree-with-given-traversals?noredirect=1&lq=1

https://cs.stackexchange.com/questions/32144/what-if-traversal-order-of-two-of-pre-order-in-order-and-post-order-is-same?rq=1

## Wikipedia

https://en.wikipedia.org/wiki/Tree_traversal#Data_structures_for_tree_traversal

https://en.wikipedia.org/wiki/Depth-first_search

https://en.wikipedia.org/wiki/Binary_relation

## Higher Education Sites

http://www.cs.usfca.edu/~brooks/S04classes/cs245/lectures/lecture11.pdf

https://www.cise.ufl.edu/~sahni/cop3530/slides/lec216.pdf