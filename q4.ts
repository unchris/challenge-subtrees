interface DomNode {
    value: string;
    children: DomNode[];
}

interface Counter {
	value: number;
}

/* Serializes a tree by preorder traversal */
function serialize(tree: DomNode): string {
	if (!tree || !tree.children) {
		// tree is invalid
		return "";
	}
	return `${tree.children.length} ` + serializePreorder(tree);

}
function serializePreorder(tree: DomNode): string {
    if (!tree) {
        return "0 ";
    }
    let serialization = tree.value + " ";
    for (var i = 0; i < tree.children.length; i++) {
    	serialization += serializePreorder(tree.children[i]);
    }
    return serialization;
}

/* Determines whether a test tree is a subtree of a main tree */
function isSubtree(mainTree: DomNode, testTree: DomNode): boolean {
	if (!testTree || !mainTree) {
		return false;
	}
	if (testTree.children.length != mainTree.children.length) {
		// explicitly not supporting this as stated in notes
		return false;
	}

	let mainSerial: string = serialize(mainTree);
	let testSerial: string = serialize(testTree);

    let mainTokens: string[] = mainSerial.split(" ");
    let testTokens: string[] = testSerial.split(" ");
 
    let j: number = 0;
    let i: number = 0;

    while (i < testTokens.length && j < mainTokens.length) {
    	if (testTokens[i] == "0" || testTokens[i] == mainTokens[j]) {
    		i++;
    	}
    	j++;
    }
    return (i == testTokens.length);
}

/* deserializes a preorder traversal */
function Tree(input: string): DomNode {
	if (!input || input.length == 0) {
		return null;
	}
	let position: Counter = {value: 0};
	let tokens: string[] = input.split(" ");
	let k: number = parseInt(tokens[position.value++]);
	return treeDeserializer(tokens, position, k);

}
function treeDeserializer(tokens: string[], position: Counter, k: number): DomNode {
	if (position.value == tokens.length) {
		return null;
	}
	if (tokens[position.value] == "0") {
		position.value++;
		return null;
	}

	let node: DomNode = {
		value: tokens[position.value++], 
		children: Array<DomNode>(k),
	}
	for (var i = 0; i < node.children.length; i++) {
		node.children[i] = treeDeserializer(tokens, position, k);
	}
	return node;
}


/* 

	BEGIN TEST SECTION 

*/

/* generic test function for arbitrary trees */
function test(testname: string, maintree: DomNode, testtree: DomNode, expectFound: boolean): void {
	let found: boolean = isSubtree(maintree, testtree);
	let condition: string = ((expectFound == found) ? "PASS" : "FAIL");
	let addendum: string = "";
	if (expectFound && !found) {
		addendum = "[expected to find a subtree]";
	} else if (!expectFound && found) {
		addendum = "[unexpectedly found a subtree]";
	}
	console.log(condition + addendum + ":" + testname);	
}

/**

	BEGIN BINARY TEST SECTION!

*/

/* one basic unit test of DomNode=>serialization=>DomNode=>serialization */
const unit: DomNode = {
	value: "a",
	children: [
		{
			value: "b",
			children: [
				{
					value: "c",
					children: [null, null]
				},
				{
					value: "d",
					children: [null, null]
				}
			]
		},
		null
	]
}

let unitSerial0: string = "2 a b c 0 0 d 0 0 0 "; // trailing space matters
console.log(`expected serialization: ${unitSerial0}`)
let unitSerial1: string = serialize(unit);
console.log(`actual serialization: ${unitSerial1}`);
let unitSerial2: string = serialize(Tree(unitSerial1));
console.log(`actual reserialization: ${unitSerial2}`);

if ((unitSerial0 === unitSerial1) && (unitSerial0 === unitSerial2)) {
	console.log("PASS:The serializations match");
} else {
	console.log("FAIL:The serializations do not match");
}

/* assignment case */
test("it must pass serialized version of original test case from assignment", 
	Tree("2 root a c g 0 0 h 0 0 d i 0 0 0 b e 0 j k 0 0 l 0 0 f 0 0 "), 
	Tree("2 a c g 0 0 h 0 0 d i 0 0 0 "), 
	true);

/* basic tests */
test("it must match the most basic tree", 
	Tree("2 a 0 0 "), 
	Tree("2 a 0 0 "), 
	true);
test("it must not match a different basic tree", 
	Tree("2 a 0 0 "), 
	Tree("2 b 0 0 "), 
	false);

test("it must match a common test case (left)", 
	Tree("2 a b 0 0 0 "), 
	Tree("2 a b 0 0 0 "), 
	true);
test("it must match a common test case (right)",
	Tree("2 a 0 b 0 0 "), 
	Tree("2 a 0 b 0 0 "), 
	true);

test("it must match a common test case (left leaf)", 
	Tree("2 a b 0 0 0 "), 
	Tree("2 b 0 0 "), 
	true);
test("it must match a common test case (right leaf)",
	Tree("2 a 0 b 0 0 "), 
	Tree("2 b 0 0 "), 
	true);

test("it must not match when a left child is on the right", 
	Tree("2 a b 0 0 0 "), 
	Tree("2 a 0 b 0 0 "), 
	false);
test("it must not match when a right child is on the left",
	Tree("2 a 0 b 0 0 "), 
	Tree("2 a b 0 0 0 "), 
	false);

test("it must match an exact tree", 
	Tree("2 a b 0 0 c 0 0 "), 
	Tree("2 a b 0 0 c 0 0 "), 
	true);
test("it must not match a mirror image ", 
	Tree("2 a c 0 0 b 0 0 "), 
	Tree("2 a b 0 0 c 0 0 "), 
	false);

test("it must find partial subtrees", 
	Tree("2 a b 0 0 c 0 0 "), 
	Tree("2 a 0 c 0 0 "), 
	true);
test("it must not match a completely different tree", 
	Tree("2 a 0 b 0 c 0 d 0 0 "), 
	Tree("2 z y 0 0 x 0 0 "), 
	false);

/* leaf node behaviour */
test("it must allow leaves in the test tree to be nodes in the main tree (left side)", 
	Tree("2 a b c d 0 0 0 0 0 "), 
	Tree("2 b c 0 0 0 "), 
	true);
test("it must allow leaves in the test tree to be nodes in the main tree (right side)", 
	Tree("2 a 0 b 0 c 0 d 0 0 "), 
	Tree("2 b 0 c 0 0 "), 
	true);

/* invalid test trees */
let mainTree: DomNode = Tree("2 a 0 b 0 c 0 d 0 0");
test("it must not match when the test tree is invalid (single null)", 
	mainTree, 
	Tree("2 0 "), 
	false);
test("it must not match when the test tree is invalid (multiple nulls)", 
	mainTree, 
	Tree("2 0 0 0 0 "), 
	false);
test("it must not match when the test tree is invalid (begins with null)", 
	mainTree, 
	Tree("2 0 b 0 c "), 
	false);

/* more complex trees positive and negative cases */
let threeLevel: DomNode = Tree("2 a b c 0 0 0 d 0 e 0 0 ");
test("it must match a subtree starting from root", 
	threeLevel, 
	Tree("2 a b c 0 0 0 d 0 0 "), 
	true);

test("it must match a subtree starting from left node", 
	threeLevel, 
	Tree("2 b c 0 0 0 "), 
	true);
test("it must not match a mirrored subtree starting from left node", 
	threeLevel, 
	Tree("2 b 0 c 0 0 "), 
	false);
test("it must match a leaf node on left side", 
	threeLevel, 
	Tree("2 c 0 0 "), 
	true);

test("it must match a subtree starting from right node", 
	threeLevel, 
	Tree("2 d 0 e 0 0 "), 
	true);
test("it must not match a mirrored subtree starting from right node", 
	threeLevel, 
	Tree("2 d e 0 0 0 "), 
	false);
test("it must match a leaf node on right side", 
	threeLevel, 
	Tree("2 e 0 0 "), 
	true);


/* this is a particular ambiguous case that I noticed with old algorithm */
test("it must not match when test tree looks like a subtree but is not",
	Tree("2 a b c d 0 0 0 0 0 "),
	Tree("2 a b c 0 0 d 0 0 0 "),
	false); 


/* tests with arbitrary token sizes */
test("it must match even with tokens of varying length (1/2)",
	Tree("2 whiskey tango 0 0 foxtrot 0 0 "),
	Tree("2 whiskey 0 foxtrot 0 0 "),
	true);

test("it must match even with tokens of varying length (2/2)",
	Tree("2 janeway pi alpha 0 0 0 0 "),
	Tree("2 pi alpha 0 0 0 "),
	true);


/**

	BEGIN K-ARY TREE TEST SECTION!

*/

/* one basic unit test of DomNode=>serialization=>DomNode=>serialization */
const unit4: DomNode = {
	value: "a",
	children: [
		{
			value: "b",
			children: [
				{
					value: "c",
					children: [null, null, null, null]
				},
				{
					value: "d",
					children: [null, null, null, null]
				},
				null, 
				null
			]
		},
		null,
		null,
		null
	]
}

let unit4Serial0: string = "4 a b c 0 0 0 0 d 0 0 0 0 0 0 0 0 0 "; // trailing space matters
console.log(`expected serialization: ${unit4Serial0}`)
let unit4Serial1: string = serialize(unit4);
console.log(`actual serialization: ${unit4Serial1}`);
let unit4Serial2: string = serialize(Tree(unit4Serial1));
console.log(`actual reserialization: ${unit4Serial2}`);

if ((unit4Serial0 === unit4Serial1) && (unit4Serial0 === unit4Serial2)) {
	console.log("PASS:The serializations match");
} else {
	console.log("FAIL:The serializations do not match");
}

/* just a root node */
mainTree = Tree("4 a 0 0 0 0 ");
test("it must match a basic 4-tree", 
	mainTree, 
	mainTree, 
	true);

test("it must not match a different basic 4-tree", 
	mainTree, 
	Tree("4 b 0 0 0 0 "), 
	false);

/* two level trees */
mainTree = Tree("4 a b 0 0 0 0 0 0 0 "); // child in position 0
test("it must children in same position (in position 0)", 
	mainTree, 
	mainTree, 
	true);
test("it must not when child position changed (in position 0)", 
	mainTree, 
	Tree("4 a 0 b 0 0 0 0 0 0 "), 
	false);
test("it must match a leaf node (in position 0)", 
	mainTree, 
	Tree("4 b 0 0 0 0 "), 
	true);
mainTree = Tree("4 a 0 b 0 0 0 0 0 0 "); // child in position 1
test("it must match children in same position (in position 1)", 
	mainTree, 
	mainTree, 
	true);
test("it must not match when child position changed (in position 1)", 
	mainTree, 
	Tree("4 a b 0 0 0 0 0 0 0 "), 
	false);
test("it must match a leaf node (in position 1)", 
	mainTree, 
	Tree("4 b 0 0 0 0 "), 
	true);
mainTree = Tree("4 a 0 0 0 b 0 0 0 0 "); // child in position 3
test("it must match children in same position (in position 3)", 
	mainTree, 
	mainTree, 
	true);
test("it must not match when child position changed (in position 3)", 
	mainTree, 
	Tree("4 a b 0 0 0 0 0 0 0 "), 
	false);
test("it must match a leaf node (in position 3)", 
	mainTree, 
	Tree("4 b 0 0 0 0 "), 
	true);

/* three level trees */
mainTree = Tree("4 a b d 0 0 0 0 e 0 0 0 0 0 0 0 c 0 0 0 0 0 ")
test("it must match self", 
	mainTree, 
	mainTree, 
	true);
test("it must match a subtree rooted at first level", 
	mainTree, 
	Tree("4 a 0 0 c 0 0 0 0 0 "), 
	true);
test("it must not match a similar subtree rooted at first level", 
	mainTree, 
	Tree("4 a 0 c 0 0 0 0 0 0 "), 
	false);
test("it must match a subtree rooted at second level", 
	mainTree, 
	Tree("4 b d 0 0 0 0 e 0 0 0 0 0 0 "), 
	true);
test("it must not match a similar subtree rooted at second level", 
	mainTree, 
	Tree("4 b d 0 0 0 0 0 e 0 0 0 0 0 "), 
	false);
test("it must match a leaf on the second level", 
	mainTree, 
	Tree("4 c 0 0 0 0 "), 
	true);
test("it must match a leaf on the third level", 
	mainTree, 
	Tree("4 d 0 0 0 0 "), 
	true);
test("it must match another leaf on the third level", 
	mainTree, 
	Tree("4 e 0 0 0 0 "), 
	true);
test("it must match a node at first level",
	mainTree,
	Tree("4 a 0 0 0 0"),
	true);
test("it must match a node at second level",
	mainTree,
	Tree("4 b 0 0 0 0"),
	true);

