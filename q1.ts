interface DomNode {
    value: string;
    left?: DomNode;
    right?: DomNode;
}

interface Counter {
	value: number;
}

/* Serializes a tree by preorder traversal */
function serializePreorder(tree: DomNode): string {
    if (!tree) {
        return "0 ";
    }
    return tree.value + " " + serializePreorder(tree.left) + serializePreorder(tree.right);
}

/* Determines whether a test tree is a subtree of a main tree */
function isSubtree(mainTree: DomNode, testTree: DomNode): boolean {
	if (!testTree || !mainTree) {
		return false;
	}

	let mainSerial: string = serializePreorder(mainTree);
	let testSerial: string = serializePreorder(testTree);

    let mainTokens: string[] = mainSerial.split(" ");
    let testTokens: string[] = testSerial.split(" ");
 
    let j: number = 0;
    let i: number = 0;
    
    while (i < testTokens.length && j < mainTokens.length) {
    	if (testTokens[i] == "0" || testTokens[i] == mainTokens[j]) {
    		i++;
    	}
    	j++;
    }
    return (i == testTokens.length);
}

/* deserializes a preorder traversal */
function Tree(input: string): DomNode {
	if (!input || input.length == 0) {
		return null;
	}
	let position: Counter = {value: 0};
	return treeDeserializer(input.split(" "), position);

}
function treeDeserializer(tokens: string[], position: Counter): DomNode {
	if (position.value == tokens.length) {
		return null;
	}
	if (tokens[position.value] == "0") {
		position.value++;
		return null;
	}

	let value: string = tokens[position.value];
	position.value++;

	return { 
		value: value, 
		left: treeDeserializer(tokens, position), 
		right: treeDeserializer(tokens, position)
	};
}

/**

	BEGIN TEST SECTION!

*/

/* original test case from assignment */
function test1(): boolean {
	// complex test case... W00t! It works!
	const dom: DomNode = {
		value: "root",
		left: {
	    	value: "a",
			left: {
	      		value: "c",
				left: {
					value: "g"
				},
				right: {
					value: "h"
				}
			},
			right: {
				value: "d",
				left: {
					value: "i"
				}
			}
		},
		right: {
			value: "b",
			left: {
	      		value: "e",
				right: {
		        	value: "j",
					left: {
						value: "k"
					},
					right: {
						value: "l"
					}
				}
			},
			right: {
				value: "f"
			}
		}
	}
	const vdom: DomNode = {
		value: "a",
		left: {
			value: "c",
			left: {
				value: "g"
			},
			right: {
				value: "h"
			}
	  	},
		right: {
			value: "d",
			left: {
				value: "i"
			}
		}
	}
	return isSubtree(dom, vdom);
}

/* generic test function for arbitrary trees */
function test(testname: string, maintree: DomNode, testtree: DomNode, expectFound: boolean): void {
	let found: boolean = isSubtree(maintree, testtree);
	let condition: string = ((expectFound == found) ? "PASS" : "FAIL");
	let addendum: string = "";
	if (expectFound && !found) {
		addendum = "[expected to find a subtree]";
	} else if (!expectFound && found) {
		addendum = "[unexpectedly found a subtree]";
	}
	console.log(condition + addendum + ":" + testname);	
}

/* one basic unit test of DomNode=>serialization=>DomNode=>serialization */
const unit: DomNode = {
		value: "a",
		left: {
			value: "b",
			left: {
				value: "c"
			},
			right: {
				value: "d"
			}
    }
}
let unitSerial0: string = "a b c 0 0 d 0 0 0 "; // trailing space matters
console.log(`expected serialization: ${unitSerial0}`)
let unitSerial1: string = serializePreorder(unit);
console.log(`actual serialization: ${unitSerial1}`);
let unitSerial2: string = serializePreorder(Tree(unitSerial1));
console.log(`actual reserialization: ${unitSerial2}`);

if ((unitSerial0 === unitSerial1) && (unitSerial0 === unitSerial2)) {
	console.log("PASS:The serializations match");
} else {
	console.log("FAIL:The serializations do not match");
}

/* assignment case */
let code: string = (test1() ? "PASS:" : "FAIL:")
console.log(`${code}:it must pass original test case from assignment (old way)`);
test("it must pass serialized version of original test case from assignment (new way)", 
	Tree("root a c g 0 0 h 0 0 d i 0 0 0 b e 0 j k 0 0 l 0 0 f 0 0 "), 
	Tree("a c g 0 0 h 0 0 d i 0 0 0 "), 
	true);

/* basic tests */
test("it must match the most basic tree", 
	Tree("a 0 0 "), 
	Tree("a 0 0 "), 
	true);
test("it must not match a different basic tree", 
	Tree("a 0 0 "), 
	Tree("b 0 0 "), 
	false);

test("it must match a common test case (left)", 
	Tree("a b 0 0 0 "), 
	Tree("a b 0 0 0 "), 
	true);
test("it must match a common test case (right)",
	Tree("a 0 b 0 0 "), 
	Tree("a 0 b 0 0 "), 
	true);

test("it must match a common test case (left leaf)", 
	Tree("a b 0 0 0 "), 
	Tree("b 0 0 "), 
	true);
test("it must match a common test case (right leaf)",
	Tree("a 0 b 0 0 "), 
	Tree("b 0 0 "), 
	true);

test("it must not match when a left child is on the right", 
	Tree("a b 0 0 0 "), 
	Tree("a 0 b 0 0 "), 
	false);
test("it must not match when a right child is on the left",
	Tree("a 0 b 0 0 "), 
	Tree("a b 0 0 0 "), 
	false);

test("it must match an exact tree", 
	Tree("a b 0 0 c 0 0 "), 
	Tree("a b 0 0 c 0 0 "), 
	true);
test("it must not match a mirror image ", 
	Tree("a c 0 0 b 0 0 "), 
	Tree("a b 0 0 c 0 0 "), 
	false);

test("it must find partial subtrees", 
	Tree("a b 0 0 c 0 0 "), 
	Tree("a 0 c 0 0 "), 
	true);
test("it must not match a completely different tree", 
	Tree("a 0 b 0 c 0 d 0 0 "), 
	Tree("z y 0 0 x 0 0 "), 
	false);

/* leaf node behaviour */
test("it must allow leaves in the test tree to be nodes in the main tree (left side)", 
	Tree("a b c d 0 0 0 0 0 "), 
	Tree("b c 0 0 0 "), 
	true);
test("it must allow leaves in the test tree to be nodes in the main tree (right side)", 
	Tree("a 0 b 0 c 0 d 0 0 "), 
	Tree("b 0 c 0 0 "), 
	true);

/* invalid test trees */
let mainTree: DomNode = Tree("a 0 b 0 c 0 d 0 0");
test("it must not match when the test tree is invalid (single null)", 
	mainTree, 
	Tree("0 "), 
	false);
test("it must not match when the test tree is invalid (multiple nulls)", 
	mainTree, 
	Tree("0 0 0 0 "), 
	false);
test("it must not match when the test tree is invalid (begins with null)", 
	mainTree, 
	Tree("0 b 0 c "), 
	false);

/* more complex trees positive and negative cases */
let threeLevel: DomNode = Tree("a b c 0 0 0 d 0 e 0 0 ");
test("it must match a subtree starting from root", 
	threeLevel, 
	Tree("a b c 0 0 0 d 0 0 "), 
	true);

test("it must match a subtree starting from left node", 
	threeLevel, 
	Tree("b c 0 0 0 "), 
	true);
test("it must not match a mirrored subtree starting from left node", 
	threeLevel, 
	Tree("b 0 c 0 0 "), 
	false);
test("it must match a leaf node on left side", 
	threeLevel, 
	Tree("c 0 0 "), 
	true);

test("it must match a subtree starting from right node", 
	threeLevel, 
	Tree("d 0 e 0 0 "), 
	true);
test("it must not match a mirrored subtree starting from right node", 
	threeLevel, 
	Tree("d e 0 0 0 "), 
	false);
test("it must match a leaf node on right side", 
	threeLevel, 
	Tree("e 0 0 "), 
	true);


/* this is a particular ambiguous case that I noticed with old algorithm */
test("it must not match when test tree looks like a subtree but is not",
	Tree("a b c d 0 0 0 0 0 "),
	Tree("a b c 0 0 d 0 0 0 "),
	false); 


/* tests with arbitrary token sizes */
test("it must match even with tokens of varying length",
	Tree("whiskey tango 0 0 foxtrot 0 0 "),
	Tree("whiskey 0 foxtrot 0 0 "),
	true);

test("it must match even with tokens of varying length",
	Tree("janeway pi alpha 0 0 0 0 "),
	Tree("pi alpha 0 0 0 "),
	true);